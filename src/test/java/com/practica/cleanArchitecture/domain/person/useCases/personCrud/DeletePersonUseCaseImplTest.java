package com.practica.cleanArchitecture.domain.person.useCases.personCrud;

import com.practica.cleanArchitecture.domain.person.entity.Person;
import com.practica.cleanArchitecture.domain.person.ports.PersonRepositoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

class DeletePersonUseCaseImplTest {
    @Mock
    private PersonRepositoryService personRepositoryService;

    @InjectMocks
    private DeletePersonUseCaseImpl deletePersonUseCase;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void execute() {
        when(personRepositoryService.doesPersonIdExists("15")).thenReturn(true);
        Person person = new Person("15");
        deletePersonUseCase.execute(person);
        verify(personRepositoryService, times(1)).doesPersonIdExists("15");
        verify(personRepositoryService, times(1)).deletePerson(person);
    }
}