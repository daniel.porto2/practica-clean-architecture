package com.practica.cleanArchitecture.domain.person.useCases.imageCrud;

import com.practica.cleanArchitecture.domain.person.entity.Person;
import com.practica.cleanArchitecture.domain.person.ports.PersonRepositoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class DeleteImageUseCaseImplTest {
    @Mock
    private PersonRepositoryService personRepositoryService;

    @InjectMocks
    private DeleteImageUseCaseImpl deleteImageUseCase;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void execute() {
        when(personRepositoryService.doesPersonHaveImage(any(Person.class))).thenReturn(true);
        Person person = new Person("15");
        deleteImageUseCase.execute(person);
        verify(personRepositoryService, times(1)).deleteImage(person);
    }
}