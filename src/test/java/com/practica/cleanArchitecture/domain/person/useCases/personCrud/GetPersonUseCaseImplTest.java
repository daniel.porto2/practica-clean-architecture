package com.practica.cleanArchitecture.domain.person.useCases.personCrud;

import com.practica.cleanArchitecture.Infraestructure.shared.util.dataTest.PersonData;
import com.practica.cleanArchitecture.domain.person.entity.Person;
import com.practica.cleanArchitecture.domain.person.ports.PersonRepositoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class GetPersonUseCaseImplTest {
    @Mock
    private PersonRepositoryService personRepositoryService;

    @InjectMocks
    private GetPersonUseCaseImpl getPersonUseCase;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void execute(){
            when(personRepositoryService.findPersonById("15")).thenReturn(PersonData.PERSON15);
            when(personRepositoryService.doesPersonIdExists("15")).thenReturn(true);
            Person person15 = new Person("15");
            Person person = getPersonUseCase.execute(person15);
            assertNotNull(person);
            assertEquals("15", person.getId());
            assertEquals("Roberto", person.getNames());
    }
}