package com.practica.cleanArchitecture.domain.person.useCases.imageCrud;

import com.practica.cleanArchitecture.Infraestructure.shared.util.dataTest.ImageData;
import com.practica.cleanArchitecture.Infraestructure.shared.util.dataTest.PersonData;
import com.practica.cleanArchitecture.domain.person.entity.Image;
import com.practica.cleanArchitecture.domain.person.entity.Person;
import com.practica.cleanArchitecture.domain.person.ports.PersonRepositoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class GetImageUseCaseImplTest {
    @Mock
    private PersonRepositoryService personRepositoryService;

    @InjectMocks
    private GetImageUseCaseImpl getImageUseCase;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void execute() {
        when(personRepositoryService.getImage(any(Person.class))).thenReturn(ImageData.IMAGE8);
        when(personRepositoryService.doesPersonHaveImage(any(Person.class))).thenReturn(true);
        Person person = new Person("15");
        Image image = getImageUseCase.execute(person);
        assertNotNull(image);
        assertEquals("8", image.getId());
        verify(personRepositoryService, times(1)).getImage(person);
    }
}