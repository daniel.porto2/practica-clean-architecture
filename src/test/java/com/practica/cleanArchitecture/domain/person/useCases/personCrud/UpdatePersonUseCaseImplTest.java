package com.practica.cleanArchitecture.domain.person.useCases.personCrud;

import com.practica.cleanArchitecture.Infraestructure.shared.util.dataTest.PersonData;
import com.practica.cleanArchitecture.domain.person.ports.PersonRepositoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

class UpdatePersonUseCaseImplTest {
    @Mock
    private PersonRepositoryService personRepositoryService;

    @InjectMocks
    private UpdatePersonUseCaseImpl updatePersonUseCase;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void execute() {
        when(personRepositoryService.doesPersonIdExists("15")).thenReturn(true);
        updatePersonUseCase.execute(PersonData.PERSON15);
        verify(personRepositoryService, times(1)).doesPersonIdExists("15");
        verify(personRepositoryService,times(1)).updatePerson(PersonData.PERSON15);
    }
}