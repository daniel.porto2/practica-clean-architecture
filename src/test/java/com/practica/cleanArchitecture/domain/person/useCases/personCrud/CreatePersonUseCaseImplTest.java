package com.practica.cleanArchitecture.domain.person.useCases.personCrud;

import com.practica.cleanArchitecture.Infraestructure.shared.util.dataTest.PersonData;
import com.practica.cleanArchitecture.domain.person.entity.Person;
import com.practica.cleanArchitecture.domain.person.ports.PersonRepositoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class CreatePersonUseCaseImplTest {
    @Mock
    private PersonRepositoryService personRepositoryService;

    @InjectMocks
    private CreatePersonUseCaseImpl createPersonUseCase;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void execute() {
        System.out.println("dd");
        when(personRepositoryService.doesDocumentNumberExists(any(String.class))).thenReturn(false);
        saveMockIncrementalId();
        Person savedPerson = createPersonUseCase.execute(PersonData.NEW_PERSON);
        assertNotNull(savedPerson);
        assertEquals("0", savedPerson.getId());
    }

    void saveMockIncrementalId() {
        when(personRepositoryService.savePerson(any(Person.class))).then(new Answer<Person>() {
            int sequence = 0;
            @Override
            public Person answer(InvocationOnMock invocation) throws Throwable{
                Person person = invocation.getArgument(0);
                sequence++;
                person.setId(Integer.toString(0));
                return person;
            }
        });
    }
}
