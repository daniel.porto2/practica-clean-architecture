package com.practica.cleanArchitecture.domain.person.useCases.imageCrud;

import com.practica.cleanArchitecture.Infraestructure.shared.util.dataTest.ImageData;
import com.practica.cleanArchitecture.domain.person.entity.Image;
import com.practica.cleanArchitecture.domain.person.entity.Person;
import com.practica.cleanArchitecture.domain.person.ports.PersonRepositoryService;
import com.practica.cleanArchitecture.domain.person.useCases.personCrud.CreatePersonUseCaseImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class AddImageUseCaseImplTest {
    @Mock
    private PersonRepositoryService personRepositoryService;

    @InjectMocks
    private AddImageUseCaseImpl addImageUseCase;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void execute() {
        when(personRepositoryService.doesPersonIdExists("15")).thenReturn(true);
        when(personRepositoryService.doesPersonHaveImage(any(Person.class))).thenReturn(false);
        saveMockIncrementalId();
        Person person = new Person("15");
        Image savedImage = addImageUseCase.execute(person, ImageData.NEW_IMAGE);
        assertNotNull(savedImage);
        assertEquals("1", savedImage.getId());
        verify(personRepositoryService, times(1)).saveImage(person);
    }

    void saveMockIncrementalId() {
        when(personRepositoryService.saveImage(any(Person.class))).then(new Answer<Image>() {
            int sequence = 0;
            @Override
            public Image answer(InvocationOnMock invocation) throws Throwable{
                Person person = invocation.getArgument(0);
                Image image = person.getImage();
                sequence++;
                image.setId(Integer.toString(sequence));
                return image;
            }
        });
    }
}