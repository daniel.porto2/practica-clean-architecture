package com.practica.cleanArchitecture.Infraestructure.configuration.repositoryServices;

import com.practica.cleanArchitecture.Infraestructure.impl.repositoryServicesImpl.PersonRepositoryServiceImpl;
import com.practica.cleanArchitecture.Infraestructure.persistence.mappers.ImagePersistenceMapper;
import com.practica.cleanArchitecture.Infraestructure.persistence.mappers.ImagePersistenceMapperImpl;
import com.practica.cleanArchitecture.Infraestructure.persistence.mappers.PersonPersistenceMapper;
import com.practica.cleanArchitecture.Infraestructure.persistence.mappers.PersonPersistenceMapperImpl;
import com.practica.cleanArchitecture.Infraestructure.persistence.repositories.ImageRepository;
import com.practica.cleanArchitecture.Infraestructure.persistence.repositories.PersonRepository;
import com.practica.cleanArchitecture.domain.person.ports.PersonRepositoryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PersonRepositoryServiceConfig {

    private ModelMapper modelMapper;

    private PersonRepository personRepository;

    private ImageRepository imageRepository;

    @Autowired
    public PersonRepositoryServiceConfig(ModelMapper modelMapper, PersonRepository personRepository, ImageRepository imageRepository) {
        this.modelMapper = modelMapper;
        this.personRepository = personRepository;
        this.imageRepository = imageRepository;
    }

    public PersonPersistenceMapper createPersonPersistenceMapper() {
        return new PersonPersistenceMapperImpl(modelMapper);
    }

    public ImagePersistenceMapper createImagePersistenceMapper() {
        return new ImagePersistenceMapperImpl(modelMapper);
    }

    @Bean
    public PersonRepositoryService createPersonRepositoryService() {
        System.out.println(personRepository);
        return new PersonRepositoryServiceImpl(personRepository,
                imageRepository,
                createPersonPersistenceMapper(),
                createImagePersistenceMapper());
    }

}
