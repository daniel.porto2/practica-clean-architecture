package com.practica.cleanArchitecture.Infraestructure.configuration.useCases.personUseCases;

import com.practica.cleanArchitecture.domain.person.ports.PersonRepositoryService;
import com.practica.cleanArchitecture.domain.person.useCases.imageCrud.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ImageCrudUseCaseConfig {

    private PersonRepositoryService personRepositoryService;

    @Autowired
    public ImageCrudUseCaseConfig(PersonRepositoryService personRepositoryService){
        this.personRepositoryService = personRepositoryService;
    }

    public AddImageUseCase addImageUseCase(){
        return new AddImageUseCaseImpl(personRepositoryService);
    }

    public GetImageUseCase getImageUseCase() {
        return new GetImageUseCaseImpl(personRepositoryService);
    }

    public UpdateImageUseCase updateImageUseCase() {
        return new UpdateImageUseCaseImpl(personRepositoryService);
    }

    public DeleteImageUseCase deleteImageUseCase() {
        return new DeleteImageUseCaseImpl(personRepositoryService);
    }
}
