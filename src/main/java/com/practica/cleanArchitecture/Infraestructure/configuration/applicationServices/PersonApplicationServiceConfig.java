package com.practica.cleanArchitecture.Infraestructure.configuration.applicationServices;

import com.practica.cleanArchitecture.Infraestructure.configuration.useCases.personUseCases.PersonCrudUseCaseConfig;
import com.practica.cleanArchitecture.Infraestructure.impl.mappersImpl.PersonMapperServiceImpl;
import com.practica.cleanArchitecture.application.ports.PersonMapperService;
import com.practica.cleanArchitecture.application.services.PersonApplicationService;
import com.practica.cleanArchitecture.application.services.PersonApplicationServiceImpl;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PersonApplicationServiceConfig {

    private ModelMapper modelMapper;

    private PersonCrudUseCaseConfig personCrudUseCaseConfig;

    public PersonMapperService personMapperService;

    @Autowired
    public PersonApplicationServiceConfig(PersonCrudUseCaseConfig personCrudUseCaseConfig, ModelMapper modelMapper) {
        this.personCrudUseCaseConfig = personCrudUseCaseConfig;
        this.modelMapper = modelMapper;
        this.personMapperService = new PersonMapperServiceImpl(modelMapper);
    }

    @Bean
    public PersonApplicationService createPersonApplicationService(){
        return new PersonApplicationServiceImpl(personMapperService,
                personCrudUseCaseConfig.createPersonUseCase(),
                personCrudUseCaseConfig.getPersonUseCase(),
                personCrudUseCaseConfig.updatePersonUseCase(),
                personCrudUseCaseConfig.deletePersonUseCase()
                );
    }
}
