package com.practica.cleanArchitecture.Infraestructure.configuration.useCases.personUseCases;

import com.practica.cleanArchitecture.domain.person.ports.PersonRepositoryService;
import com.practica.cleanArchitecture.domain.person.useCases.personCrud.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PersonCrudUseCaseConfig {

    PersonRepositoryService personRepositoryService;

    @Autowired
    public PersonCrudUseCaseConfig(PersonRepositoryService personRepositoryService){
        this.personRepositoryService = personRepositoryService;
    }

    public CreatePersonUseCase createPersonUseCase(){
        System.out.println("************************************************");
        System.out.println(personRepositoryService);
        return new CreatePersonUseCaseImpl(personRepositoryService);
    }

    public GetPersonUseCase getPersonUseCase(){
        return new GetPersonUseCaseImpl(personRepositoryService);
    }

    public UpdatePersonUseCase updatePersonUseCase() {
        return new UpdatePersonUseCaseImpl(personRepositoryService);
    }

    public DeletePersonUseCase deletePersonUseCase(){
        return new DeletePersonUseCaseImpl(personRepositoryService);
    }
}
