package com.practica.cleanArchitecture.Infraestructure.configuration.applicationServices;

import com.practica.cleanArchitecture.Infraestructure.configuration.useCases.personUseCases.ImageCrudUseCaseConfig;
import com.practica.cleanArchitecture.Infraestructure.impl.mappersImpl.ImageMapperServiceImpl;
import com.practica.cleanArchitecture.application.ports.ImageMapperService;
import com.practica.cleanArchitecture.application.services.ImageApplicationService;
import com.practica.cleanArchitecture.application.services.ImageApplicationServiceImpl;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ImageApplicationServiceConfig {
    private ModelMapper modelMapper;

    private ImageCrudUseCaseConfig imageCrudUseCaseConfig;

    private ImageMapperService imageMapperService;

    @Autowired
    public ImageApplicationServiceConfig(ModelMapper modelMapper, ImageCrudUseCaseConfig imageCrudUseCaseConfig) {
        this.modelMapper = modelMapper;
        this.imageCrudUseCaseConfig = imageCrudUseCaseConfig;
        this.imageMapperService = new ImageMapperServiceImpl(modelMapper);
    }

    @Bean
    public ImageApplicationService createImageApplicationService() {
        return new ImageApplicationServiceImpl(imageMapperService,
                imageCrudUseCaseConfig.addImageUseCase(),
                imageCrudUseCaseConfig.getImageUseCase(),
                imageCrudUseCaseConfig.updateImageUseCase(),
                imageCrudUseCaseConfig.deleteImageUseCase());
    }
}
