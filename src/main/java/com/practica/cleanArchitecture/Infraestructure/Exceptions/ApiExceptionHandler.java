package com.practica.cleanArchitecture.Infraestructure.Exceptions;

import com.practica.cleanArchitecture.domain.shared.mainExceptions.DuplicatedFieldException;
import com.practica.cleanArchitecture.domain.shared.mainExceptions.NotFoundException;
import org.apache.tomcat.util.http.fileupload.impl.SizeLimitExceededException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.support.MissingServletRequestPartException;

@ControllerAdvice
public class ApiExceptionHandler {
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({
            NotFoundException.class
    })
    @ResponseBody
    public ErrorMessage notFoundRequest(Exception exception){
        System.out.println(exception);
        return new ErrorMessage(exception, HttpStatus.NOT_FOUND.value());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({
            org.springframework.http.converter.HttpMessageNotReadableException.class,
            java.sql.SQLIntegrityConstraintViolationException.class,
            NullPointerException.class,
            SizeLimitExceededException.class,
            MissingServletRequestPartException.class,
            DuplicatedFieldException.class
    })
    @ResponseBody
    public ErrorMessage badRequest(Exception exception) {
        ErrorMessage errorMessage= new ErrorMessage(exception, HttpStatus.BAD_REQUEST.value());
        System.out.println(errorMessage);
        return errorMessage;
    }
}
