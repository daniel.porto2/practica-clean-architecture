package com.practica.cleanArchitecture.Infraestructure.persistence.repositories;
import com.practica.cleanArchitecture.Infraestructure.persistence.entities.PersonPersistenceEntity;
import org.springframework.data.repository.CrudRepository;

public interface PersonRepository extends CrudRepository<PersonPersistenceEntity, Long> {
    public boolean existsByDocumentNumber(String documentNumber);

    public boolean existsById(Long id);
}
