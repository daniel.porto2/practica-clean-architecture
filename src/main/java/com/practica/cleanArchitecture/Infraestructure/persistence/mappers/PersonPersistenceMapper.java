package com.practica.cleanArchitecture.Infraestructure.persistence.mappers;

import com.practica.cleanArchitecture.Infraestructure.persistence.entities.PersonPersistenceEntity;
import com.practica.cleanArchitecture.domain.person.entity.Person;

public interface PersonPersistenceMapper {
    public PersonPersistenceEntity personToPersonPersistenceEntity(Person person);

    public Person personPersistenceEntityToPerson(PersonPersistenceEntity person);

}
