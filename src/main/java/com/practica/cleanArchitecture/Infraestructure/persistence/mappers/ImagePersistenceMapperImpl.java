package com.practica.cleanArchitecture.Infraestructure.persistence.mappers;

import com.practica.cleanArchitecture.Infraestructure.persistence.entities.ImagePersistenceEntity;
import com.practica.cleanArchitecture.domain.person.entity.Image;
import com.practica.cleanArchitecture.domain.person.entity.Person;
import org.modelmapper.ModelMapper;

public class ImagePersistenceMapperImpl implements ImagePersistenceMapper {

    private final ModelMapper modelMapper;

    public ImagePersistenceMapperImpl(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public ImagePersistenceEntity personToImagePersistenceEntity(Person person) {
        ImagePersistenceEntity ipe = modelMapper.typeMap(Image.class, ImagePersistenceEntity.class).map(person.getImage());
        ipe.setPersonId(Long.parseLong(person.getId()));
        return ipe;
    }


    @Override
    public Image imagePersistenceEntityToImage(ImagePersistenceEntity imagePersistenceEntity) {
        return modelMapper.typeMap(ImagePersistenceEntity.class, Image.class).map(imagePersistenceEntity);
    }
}
