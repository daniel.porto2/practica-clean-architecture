package com.practica.cleanArchitecture.Infraestructure.persistence.mappers;

import com.practica.cleanArchitecture.Infraestructure.persistence.entities.ImagePersistenceEntity;
import com.practica.cleanArchitecture.domain.person.entity.Image;
import com.practica.cleanArchitecture.domain.person.entity.Person;

public interface ImagePersistenceMapper {
    public ImagePersistenceEntity personToImagePersistenceEntity(Person image);

    public Image imagePersistenceEntityToImage(ImagePersistenceEntity image);
}
