package com.practica.cleanArchitecture.Infraestructure.persistence.mappers;

import com.practica.cleanArchitecture.Infraestructure.persistence.entities.PersonPersistenceEntity;
import com.practica.cleanArchitecture.domain.person.entity.Person;
import org.modelmapper.ModelMapper;

public class PersonPersistenceMapperImpl implements PersonPersistenceMapper {

    private final ModelMapper modelMapper;

    public PersonPersistenceMapperImpl(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public PersonPersistenceEntity personToPersonPersistenceEntity(Person person) {
        return modelMapper.typeMap(Person.class, PersonPersistenceEntity.class).map(person);
    }

    @Override
    public Person personPersistenceEntityToPerson(PersonPersistenceEntity person) {
        return modelMapper.typeMap(PersonPersistenceEntity.class, Person.class).map(person);
    }
}
