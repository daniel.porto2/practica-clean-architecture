package com.practica.cleanArchitecture.Infraestructure.persistence.entities;

import com.practica.cleanArchitecture.domain.person.entity.DocumentTypeEnum;
import com.practica.cleanArchitecture.domain.person.entity.SexEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(
        name="persons",
        schema = "personsSchema",
        indexes = {@Index(name = "documentNumber_index", columnList = "document_number", unique = true)}
)
public class PersonPersistenceEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "document_number", nullable = false, length = 20)
    private String  documentNumber;

    @Column(name = "names")
    private String names;

    @Column(name = "surnames")
    private String surnames;

    @Column(name = "countryOfOrigin")
    private String countryOfOrigin;

    @Column(name = "documentType")
    private String documentType;

    @Column(name = "age")
    private int age;

    @Column(name = "sex")
    private String sex;

}
