package com.practica.cleanArchitecture.Infraestructure.persistence.entities;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.*;

@Document(collection = "images")
@Getter
@Setter
public class ImagePersistenceEntity {
    @Id
    private String id;

    @Column(name = "personId")
    @Indexed(unique = true)
    private long personId;

    @Column(name = "file")
    private byte[] file;
}
