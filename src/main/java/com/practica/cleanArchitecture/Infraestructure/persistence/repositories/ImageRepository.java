package com.practica.cleanArchitecture.Infraestructure.persistence.repositories;

import com.practica.cleanArchitecture.Infraestructure.persistence.entities.ImagePersistenceEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface ImageRepository extends MongoRepository<ImagePersistenceEntity, String> {
    public Optional<ImagePersistenceEntity> findByPersonId(Long personId);

    public boolean existsByPersonId(Long personId);

    public void deleteByPersonId(Long personId);
}
