package com.practica.cleanArchitecture.Infraestructure.shared.util.dataTest;

import com.practica.cleanArchitecture.domain.person.entity.DocumentTypeEnum;
import com.practica.cleanArchitecture.domain.person.entity.Person;
import com.practica.cleanArchitecture.domain.person.entity.SexEnum;

public class PersonData {
    public static Person NEW_PERSON = new Person( null, "548940890", "Zara", "Martínez", "España", DocumentTypeEnum.CE, 25, SexEnum.F, null);

    public static Person PERSON15 = new Person( "15" , "4780484974", "Roberto", "Gonzales", "Colombia", DocumentTypeEnum.CC, 36, SexEnum.M, null);

}
