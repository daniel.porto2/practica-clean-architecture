package com.practica.cleanArchitecture.Infraestructure.shared.util;

public class Routes {
    public static final String baseRoute = "/api";
    public static final String personsRoute = baseRoute+"/persons";
    public static final String imageRoute = personsRoute+"/{personId}/image";
}
