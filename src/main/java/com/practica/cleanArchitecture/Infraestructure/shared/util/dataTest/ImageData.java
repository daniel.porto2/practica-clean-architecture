package com.practica.cleanArchitecture.Infraestructure.shared.util.dataTest;

import com.practica.cleanArchitecture.domain.person.entity.Image;

import java.nio.charset.StandardCharsets;

public class ImageData {
    public static Image NEW_IMAGE = new Image(null, "e04fd020ea3a6910a2d808002b30309d".getBytes(StandardCharsets.UTF_8));

    public static Image IMAGE8 = new Image("8", "F43RTF8374F853445F4346F46F".getBytes(StandardCharsets.UTF_8));

}
