package com.practica.cleanArchitecture.Infraestructure.delivery;

import com.practica.cleanArchitecture.application.dtos.image.ImageCreationDTO;
import com.practica.cleanArchitecture.application.dtos.image.ImageResponseDTO;
import com.practica.cleanArchitecture.application.dtos.image.ImageUpdateDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

public interface ImageController {
    public ResponseEntity<ImageResponseDTO> addImage(String personId, MultipartFile file, ImageCreationDTO imageCreationDTO);

    public ResponseEntity<ImageResponseDTO> getImage(String personId);

    public ResponseEntity<Void> updateImage(String personId, MultipartFile file, ImageUpdateDTO imageUpdateDTO);

    public ResponseEntity<Void> deleteImage(String personId);
}
