package com.practica.cleanArchitecture.Infraestructure.delivery;

import com.practica.cleanArchitecture.application.dtos.person.PersonCreationDTO;
import com.practica.cleanArchitecture.application.dtos.person.PersonResponseDto;
import com.practica.cleanArchitecture.application.dtos.person.PersonUpdateDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

public interface PersonController {
    public ResponseEntity<Void> createPerson(PersonCreationDTO personCreationDTO, UriComponentsBuilder ucBuilder);

    public ResponseEntity<PersonResponseDto> getPerson(String id);

    public ResponseEntity<Void> updatePerson(String id, PersonUpdateDTO personUpdateDTO);

    public ResponseEntity<Void> deletePerson(String id);
}
