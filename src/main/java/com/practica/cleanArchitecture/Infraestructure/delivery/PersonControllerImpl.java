package com.practica.cleanArchitecture.Infraestructure.delivery;

import com.practica.cleanArchitecture.Infraestructure.shared.util.Routes;
import com.practica.cleanArchitecture.application.dtos.person.PersonCreationDTO;
import com.practica.cleanArchitecture.application.dtos.person.PersonResponseDto;
import com.practica.cleanArchitecture.application.dtos.person.PersonUpdateDTO;
import com.practica.cleanArchitecture.application.services.PersonApplicationService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import javax.validation.Valid;

@RestController
@RequestMapping(Routes.personsRoute)
public class PersonControllerImpl implements PersonController{

    private final PersonApplicationService personApplicationService;

    public PersonControllerImpl(PersonApplicationService personApplicationService) {
        this.personApplicationService = personApplicationService;
    }

    @Override
    @PostMapping
    public ResponseEntity<Void> createPerson(@Valid @RequestBody PersonCreationDTO personCreationDTO, UriComponentsBuilder ucBuilder) {
        System.out.println(personCreationDTO);
        PersonResponseDto personResponseDto = personApplicationService.crete(personCreationDTO);
        System.out.println(personResponseDto);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/{id}").buildAndExpand(personResponseDto.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    @Override
    @GetMapping("/{id}")
    public ResponseEntity<PersonResponseDto> getPerson(@PathVariable("id") String id) {
        PersonResponseDto person = personApplicationService.getPersonById(id);
        return new ResponseEntity<PersonResponseDto>(person, HttpStatus.OK);
    }

    @Override
    @PutMapping("/{id}")
    public ResponseEntity<Void> updatePerson(@PathVariable("id") String id, @Valid @RequestBody PersonUpdateDTO personUpdateDTO) {
        personApplicationService.update(id, personUpdateDTO);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Override
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePerson(@PathVariable("id") String id) {
        personApplicationService.deletePersonById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
