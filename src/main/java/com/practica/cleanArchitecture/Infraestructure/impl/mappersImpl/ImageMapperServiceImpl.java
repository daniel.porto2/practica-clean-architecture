package com.practica.cleanArchitecture.Infraestructure.impl.mappersImpl;

import com.practica.cleanArchitecture.application.dtos.image.ImageCreationDTO;
import com.practica.cleanArchitecture.application.dtos.image.ImageResponseDTO;
import com.practica.cleanArchitecture.application.dtos.image.ImageUpdateDTO;
import com.practica.cleanArchitecture.application.ports.ImageMapperService;
import com.practica.cleanArchitecture.domain.person.entity.Image;
import org.modelmapper.ModelMapper;

import java.io.IOException;

public class ImageMapperServiceImpl implements ImageMapperService {

    private final ModelMapper modelMapper;

    public ImageMapperServiceImpl(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public Image imageCreationDtoToImage(ImageCreationDTO imageCreationDTO) {
        return modelMapper.typeMap(ImageCreationDTO.class, Image.class)
                .addMapping(imgDto -> {
                    try {
                        return imgDto.getFile().getBytes();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return imgDto;
                }, Image::setFile).map(imageCreationDTO);
    }

    @Override
    public ImageResponseDTO imageToImageResponseDto(Image image) {
        return modelMapper.typeMap(Image.class, ImageResponseDTO.class).map(image);
    }

    @Override
    public Image ImageUpdateDtoToImage(ImageUpdateDTO imageUpdateDTO) {
        return modelMapper.typeMap(ImageUpdateDTO.class, Image.class)
                .addMapping(imgDto -> {
                    try {
                        return imgDto.getFile().getBytes();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return imgDto;
                }, Image::setFile).map(imageUpdateDTO);
    }

    @Override
    public Image imageToImage(Image image1, Image image2) {
        modelMapper.typeMap(Image.class, Image.class).addMappings(mpr -> mpr.skip(Image::setId))
                .map(image1, image2);
        System.out.println(image2);
        return image2;
    }
}
