package com.practica.cleanArchitecture.Infraestructure.impl.mappersImpl;

import com.practica.cleanArchitecture.application.dtos.person.PersonCreationDTO;
import com.practica.cleanArchitecture.application.dtos.person.PersonResponseDto;
import com.practica.cleanArchitecture.application.dtos.person.PersonUpdateDTO;
import com.practica.cleanArchitecture.application.ports.PersonMapperService;
import com.practica.cleanArchitecture.domain.person.entity.Person;
import org.modelmapper.ModelMapper;

public class PersonMapperServiceImpl implements PersonMapperService {

    private final ModelMapper modelMapper;

    public PersonMapperServiceImpl(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public Person personCreationDtoToPersona(PersonCreationDTO personCreationDTO) {
        personCreationDTO.setDocumentType(personCreationDTO.getDocumentType().toUpperCase());
        personCreationDTO.setSex(personCreationDTO.getSex().toUpperCase());
        return modelMapper.typeMap(PersonCreationDTO.class, Person.class).map(personCreationDTO);
    }

    @Override
    public Person personUpdateDtoToPerson(PersonUpdateDTO personUpdateDTO) {
        personUpdateDTO.setDocumentType(personUpdateDTO.getDocumentType().toUpperCase());
        personUpdateDTO.setSex(personUpdateDTO.getSex().toUpperCase());
        return modelMapper.typeMap(PersonUpdateDTO.class, Person.class).map(personUpdateDTO);
    }

    @Override
    public Person personToPerson(Person person1, Person person2) {
        modelMapper.typeMap(Person.class, Person.class).
                addMappings(mpr -> mpr.skip(Person::setId))
                .map(person1, person2);
        return person2;
    }

    @Override
    public PersonResponseDto personToPersonResponseDto(Person person) {
        return modelMapper.typeMap(Person.class, PersonResponseDto.class).map(person);
    }


}
