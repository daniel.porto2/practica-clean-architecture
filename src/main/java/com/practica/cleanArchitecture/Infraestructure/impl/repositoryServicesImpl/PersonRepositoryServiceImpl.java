package com.practica.cleanArchitecture.Infraestructure.impl.repositoryServicesImpl;

import com.practica.cleanArchitecture.Infraestructure.persistence.entities.ImagePersistenceEntity;
import com.practica.cleanArchitecture.Infraestructure.persistence.mappers.ImagePersistenceMapper;
import com.practica.cleanArchitecture.Infraestructure.persistence.mappers.PersonPersistenceMapper;
import com.practica.cleanArchitecture.Infraestructure.persistence.entities.PersonPersistenceEntity;
import com.practica.cleanArchitecture.Infraestructure.persistence.repositories.ImageRepository;
import com.practica.cleanArchitecture.Infraestructure.persistence.repositories.PersonRepository;
import com.practica.cleanArchitecture.domain.person.entity.Image;
import com.practica.cleanArchitecture.domain.person.entity.Person;
import com.practica.cleanArchitecture.domain.person.ports.PersonRepositoryService;

import java.util.Optional;

public class PersonRepositoryServiceImpl implements PersonRepositoryService {

    private final PersonRepository personRepository;

    private final ImageRepository imageRepository;

    private final PersonPersistenceMapper personPersistenceMapper;

    private final ImagePersistenceMapper imagePersistenceMapper;

    public PersonRepositoryServiceImpl(PersonRepository personRepository, ImageRepository imageRepository, PersonPersistenceMapper personPersistenceMapper, ImagePersistenceMapper imagePersistenceMapper) {
        this.personRepository = personRepository;
        this.imageRepository = imageRepository;
        this.personPersistenceMapper = personPersistenceMapper;
        this.imagePersistenceMapper = imagePersistenceMapper;
    }

    @Override
    public Person savePerson(Person person) {
        PersonPersistenceEntity ppe = personPersistenceMapper.personToPersonPersistenceEntity(person);
        ppe = personRepository.save(ppe);
        System.out.println(ppe);
        Person savedPerson = personPersistenceMapper.personPersistenceEntityToPerson(ppe);
        System.out.println(savedPerson);
        return savedPerson;
    }

    @Override
    public boolean doesDocumentNumberExists(String documentNumber) {
        return personRepository.existsByDocumentNumber(documentNumber);
    }

    @Override
    public boolean doesPersonIdExists(String id) {
        return personRepository.existsById(Long.parseLong(id));
    }

    @Override
    public Person findPersonById(String id) {
        Optional<PersonPersistenceEntity> ppe = personRepository.findById(Long.parseLong(id));
        return personPersistenceMapper.personPersistenceEntityToPerson(ppe.get());
    }

    @Override
    public void updatePerson(Person person) {
        PersonPersistenceEntity ppe = personPersistenceMapper.personToPersonPersistenceEntity(person);
        personRepository.save(ppe);
    }

    @Override
    public void deletePerson(Person person) {
        PersonPersistenceEntity ppe = personPersistenceMapper.personToPersonPersistenceEntity(person);
        personRepository.delete(ppe);
    }

    @Override
    public boolean doesPersonHaveImage(Person person) {
        return imageRepository.existsByPersonId(Long.parseLong(person.getId()));
    }

    @Override
    public Image saveImage(Person person) {
        Image image = person.getImage();
        ImagePersistenceEntity ipe = imagePersistenceMapper.personToImagePersistenceEntity(person);
        ipe = imageRepository.save(ipe);
        return imagePersistenceMapper.imagePersistenceEntityToImage(ipe);
    }

    @Override
    public Image getImage(Person person) {
        ImagePersistenceEntity ipe = imageRepository.findByPersonId(Long.parseLong(person.getId())).get();
        System.out.println(ipe);
        return imagePersistenceMapper.imagePersistenceEntityToImage(ipe);
    }

    @Override
    public void updateImage(Person person) {
        ImagePersistenceEntity ipe = imagePersistenceMapper.personToImagePersistenceEntity(person);
        System.out.println(ipe);
        imageRepository.save(ipe);
    }

    @Override
    public void deleteImage(Person person) {
        imageRepository.deleteByPersonId(Long.parseLong(person.getId()));
    }
}
