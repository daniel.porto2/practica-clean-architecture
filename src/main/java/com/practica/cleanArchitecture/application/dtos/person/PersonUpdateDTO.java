package com.practica.cleanArchitecture.application.dtos.person;

import lombok.*;

@Getter
@Setter
@ToString(includeFieldNames=true)
@AllArgsConstructor
@NoArgsConstructor
public class PersonUpdateDTO {

    private String documentType;

    private String documentNumber;

    private String names;

    private String surnames;

    private Integer age;

    private String countryOfOrigin;

    private String sex;
}
