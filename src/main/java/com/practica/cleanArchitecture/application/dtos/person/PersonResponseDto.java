package com.practica.cleanArchitecture.application.dtos.person;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonResponseDto {

    private long id;

    private String documentType;

    private String documentNumber;

    private String names;

    private String surnames;

    private Integer age;

    private String countryOfOrigin;

    private String sex;
}
