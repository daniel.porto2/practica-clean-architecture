package com.practica.cleanArchitecture.application.dtos.person;

import lombok.*;

@Getter
@Setter
@ToString(includeFieldNames=true)
@AllArgsConstructor
public class PersonCreationDTO {

    @NonNull
    private String documentType;

    @NonNull
    private String documentNumber;

    @NonNull
    private String names;

    @NonNull
    private String surnames;

    @NonNull
    private Integer age;

    private String countryOfOrigin;

    private String sex;
}
