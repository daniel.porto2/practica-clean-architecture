package com.practica.cleanArchitecture.application.services;

import com.practica.cleanArchitecture.application.dtos.person.PersonCreationDTO;
import com.practica.cleanArchitecture.application.dtos.person.PersonResponseDto;
import com.practica.cleanArchitecture.application.dtos.person.PersonUpdateDTO;
import com.practica.cleanArchitecture.domain.person.entity.Person;

public interface PersonApplicationService {
    public PersonResponseDto crete(PersonCreationDTO personCreationDTO);

    public PersonResponseDto getPersonById(String id);

    public void update(String id, PersonUpdateDTO personUpdateDTO);

    public void deletePersonById(String id);
}
