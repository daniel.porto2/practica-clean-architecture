package com.practica.cleanArchitecture.application.services;

import com.practica.cleanArchitecture.application.dtos.image.ImageCreationDTO;
import com.practica.cleanArchitecture.application.dtos.image.ImageResponseDTO;
import com.practica.cleanArchitecture.application.dtos.image.ImageUpdateDTO;
import com.practica.cleanArchitecture.application.ports.ImageMapperService;
import com.practica.cleanArchitecture.domain.person.entity.Image;
import com.practica.cleanArchitecture.domain.person.entity.Person;
import com.practica.cleanArchitecture.domain.person.useCases.imageCrud.AddImageUseCase;
import com.practica.cleanArchitecture.domain.person.useCases.imageCrud.DeleteImageUseCase;
import com.practica.cleanArchitecture.domain.person.useCases.imageCrud.GetImageUseCase;
import com.practica.cleanArchitecture.domain.person.useCases.imageCrud.UpdateImageUseCase;

public class ImageApplicationServiceImpl implements ImageApplicationService{

    private final ImageMapperService imageMapperService;

    private final AddImageUseCase addImageUseCase;

    private final GetImageUseCase getImageUseCase;

    private final UpdateImageUseCase updateImageUseCase;

    private final DeleteImageUseCase deleteImageUseCase;

    public ImageApplicationServiceImpl(ImageMapperService imageMapperService, AddImageUseCase addImageUseCase, GetImageUseCase getImageUseCase, UpdateImageUseCase updateImageUseCase, DeleteImageUseCase deleteImageUseCase) {
        this.imageMapperService = imageMapperService;
        this.addImageUseCase = addImageUseCase;
        this.getImageUseCase = getImageUseCase;
        this.updateImageUseCase = updateImageUseCase;
        this.deleteImageUseCase = deleteImageUseCase;
    }

    @Override
    public ImageResponseDTO addImage(String personId, ImageCreationDTO imageCreationDTO) {
        Image image = imageMapperService.imageCreationDtoToImage(imageCreationDTO);
        Person person = new Person(personId);
        Image savedImage = addImageUseCase.execute(person, image);
        return imageMapperService.imageToImageResponseDto(savedImage);
    }

    @Override
    public ImageResponseDTO getImageByPersonId(String personId) {
        Person person = new Person(personId);
        Image imageResult = getImageUseCase.execute(person);
        return imageMapperService.imageToImageResponseDto(imageResult);
    }

    @Override
    public void updateImage(String personId, ImageUpdateDTO imageUpdateDTO) {
        Image tempImage = new Image();
        Person person = new Person(personId);
        Image oldImage = getImageUseCase.execute(person);
        Image newImage = imageMapperService.ImageUpdateDtoToImage(imageUpdateDTO);
        Image imageUpdatedFields = imageMapperService.imageToImage(newImage, oldImage);
        updateImageUseCase.execute(person, imageUpdatedFields);
    }

    @Override
    public void deleteImageByPersonId(String personId) {
        Person person = new Person(personId);
        deleteImageUseCase.execute(person);
    }
}
