package com.practica.cleanArchitecture.application.services;

import com.practica.cleanArchitecture.application.dtos.person.PersonCreationDTO;
import com.practica.cleanArchitecture.application.dtos.person.PersonResponseDto;
import com.practica.cleanArchitecture.application.dtos.person.PersonUpdateDTO;
import com.practica.cleanArchitecture.application.ports.PersonMapperService;
import com.practica.cleanArchitecture.domain.person.entity.Person;
import com.practica.cleanArchitecture.domain.person.useCases.personCrud.*;

import java.util.Optional;

public class PersonApplicationServiceImpl implements PersonApplicationService{
    private final PersonMapperService personMapperService;

    private final CreatePersonUseCase createPersonUseCase;

    private final GetPersonUseCase getPersonUseCase;

    private  final UpdatePersonUseCase updatePersonUseCase;

    private final DeletePersonUseCase deletePersonUseCase;

    public PersonApplicationServiceImpl(PersonMapperService personMapperService, CreatePersonUseCase createPersonUseCase, GetPersonUseCase getPersonUseCase, UpdatePersonUseCase updatePersonUseCase, DeletePersonUseCase deletePersonUseCase) {
        this.personMapperService = personMapperService;
        this.createPersonUseCase = createPersonUseCase;
        this.getPersonUseCase = getPersonUseCase;
        this.updatePersonUseCase = updatePersonUseCase;
        this.deletePersonUseCase = deletePersonUseCase;
    }

    @Override
    public PersonResponseDto crete(PersonCreationDTO personCreationDTO) {
        Person person = createPersonUseCase.execute(personMapperService.personCreationDtoToPersona(personCreationDTO));
        System.out.println(person);
        return personMapperService.personToPersonResponseDto(person);
    }

    @Override
    public PersonResponseDto getPersonById(String id) {
        Person person = getPersonUseCase.execute(new Person(id));
        return personMapperService.personToPersonResponseDto(person);
    }

    @Override
    public void update(String id, PersonUpdateDTO personUpdateDTO) {
        Person oldPerson = getPersonUseCase.execute(new Person(id));
        Person newPerson = personMapperService.personUpdateDtoToPerson(personUpdateDTO);
        Person personUpdatedFields = personMapperService.personToPerson(newPerson, oldPerson);
        updatePersonUseCase.execute(personUpdatedFields);
    }

    @Override
    public void deletePersonById(String id) {
        deletePersonUseCase.execute(new Person(id));
    }
}
