package com.practica.cleanArchitecture.application.services;

import com.practica.cleanArchitecture.application.dtos.image.ImageCreationDTO;
import com.practica.cleanArchitecture.application.dtos.image.ImageResponseDTO;
import com.practica.cleanArchitecture.application.dtos.image.ImageUpdateDTO;
import com.practica.cleanArchitecture.domain.person.entity.Image;

public interface ImageApplicationService {
    public ImageResponseDTO addImage(String personId, ImageCreationDTO imageCreationDTO);

    public ImageResponseDTO getImageByPersonId(String personId);

    public void updateImage(String personId, ImageUpdateDTO image);

    public void deleteImageByPersonId(String personId);
}
