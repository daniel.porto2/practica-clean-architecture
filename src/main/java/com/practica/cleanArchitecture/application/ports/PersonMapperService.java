package com.practica.cleanArchitecture.application.ports;

import com.practica.cleanArchitecture.Infraestructure.persistence.entities.ImagePersistenceEntity;
import com.practica.cleanArchitecture.application.dtos.person.PersonCreationDTO;
import com.practica.cleanArchitecture.application.dtos.person.PersonResponseDto;
import com.practica.cleanArchitecture.application.dtos.person.PersonUpdateDTO;
import com.practica.cleanArchitecture.domain.person.entity.Person;

public interface PersonMapperService {
    public Person personCreationDtoToPersona(PersonCreationDTO personCreationDTO);

    public Person personUpdateDtoToPerson(PersonUpdateDTO personUpdateDTO);

    public Person personToPerson(Person person1, Person person2);

    public PersonResponseDto personToPersonResponseDto(Person person);
}
