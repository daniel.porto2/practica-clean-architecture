package com.practica.cleanArchitecture.application.ports;

import com.practica.cleanArchitecture.application.dtos.image.ImageCreationDTO;
import com.practica.cleanArchitecture.application.dtos.image.ImageResponseDTO;
import com.practica.cleanArchitecture.application.dtos.image.ImageUpdateDTO;
import com.practica.cleanArchitecture.domain.person.entity.Image;

public interface ImageMapperService {
    public Image imageCreationDtoToImage(ImageCreationDTO imageCreationDTO);

    public ImageResponseDTO imageToImageResponseDto(Image image);

    public Image ImageUpdateDtoToImage(ImageUpdateDTO imageUpdateDTO);

    public Image imageToImage(Image image1, Image image2);
}
