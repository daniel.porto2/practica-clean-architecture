package com.practica.cleanArchitecture.domain.person.entity;

import com.practica.cleanArchitecture.domain.person.entity.Person;

import java.util.UUID;

public class Image {
    private String id;

    private byte[] file;

    public Image(String id){
        this.id = id;
    }

    public Image() {

    }

    public Image(String id, byte[] file) {
        this.id = id;
        this.file = file;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }
}
