package com.practica.cleanArchitecture.domain.person.useCases.personCrud;

import com.practica.cleanArchitecture.domain.person.entity.Person;

public interface CreatePersonUseCase {
    public Person execute(Person person);
}
