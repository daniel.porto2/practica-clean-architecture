package com.practica.cleanArchitecture.domain.person.entity;

public enum DocumentTypeEnum {
    CC, CE, NIP, NIT, TI, PAP
}
