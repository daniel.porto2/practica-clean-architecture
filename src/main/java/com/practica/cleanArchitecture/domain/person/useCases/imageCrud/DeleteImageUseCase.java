package com.practica.cleanArchitecture.domain.person.useCases.imageCrud;

import com.practica.cleanArchitecture.domain.person.entity.Image;
import com.practica.cleanArchitecture.domain.person.entity.Person;

public interface DeleteImageUseCase {
    public void execute(Person person);
}
