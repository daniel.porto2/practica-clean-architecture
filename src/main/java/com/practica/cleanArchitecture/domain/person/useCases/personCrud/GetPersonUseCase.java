package com.practica.cleanArchitecture.domain.person.useCases.personCrud;

import com.practica.cleanArchitecture.domain.person.entity.Person;

import java.util.Optional;
import java.util.UUID;

public interface GetPersonUseCase {
    public Person execute(Person person);
}
