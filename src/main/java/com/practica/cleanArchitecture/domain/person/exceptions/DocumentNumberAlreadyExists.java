package com.practica.cleanArchitecture.domain.person.exceptions;

import com.practica.cleanArchitecture.domain.shared.mainExceptions.DuplicatedFieldException;

public class DocumentNumberAlreadyExists extends DuplicatedFieldException {
    public static final String DESCRIPTION = "Document number already exists";

    public DocumentNumberAlreadyExists() {
        super(DESCRIPTION);
    }

    public DocumentNumberAlreadyExists(String detail) {
        super(DESCRIPTION + ". " + detail);
    }
}
