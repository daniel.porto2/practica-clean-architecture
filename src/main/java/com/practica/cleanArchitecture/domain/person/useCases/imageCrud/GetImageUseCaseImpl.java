package com.practica.cleanArchitecture.domain.person.useCases.imageCrud;

import com.practica.cleanArchitecture.domain.person.entity.Image;
import com.practica.cleanArchitecture.domain.person.entity.Person;
import com.practica.cleanArchitecture.domain.person.exceptions.ImageNotFoundException;
import com.practica.cleanArchitecture.domain.person.ports.PersonRepositoryService;

public class GetImageUseCaseImpl implements GetImageUseCase{

    private final PersonRepositoryService personRepositoryService;

    public GetImageUseCaseImpl(PersonRepositoryService personRepositoryService) {
        this.personRepositoryService = personRepositoryService;
    }

    @Override
    public Image execute(Person person) {
        if(!personRepositoryService.doesPersonHaveImage(person)){
            throw new ImageNotFoundException("Image of person with id " + person.getId() + " not found");
        }
        return personRepositoryService.getImage(person);
    }
}
