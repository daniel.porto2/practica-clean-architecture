package com.practica.cleanArchitecture.domain.person.useCases.personCrud;

import com.practica.cleanArchitecture.domain.person.entity.Person;
import com.practica.cleanArchitecture.domain.person.exceptions.PersonIdNotFoundException;
import com.practica.cleanArchitecture.domain.person.ports.PersonRepositoryService;

public class UpdatePersonUseCaseImpl implements UpdatePersonUseCase{
    private final PersonRepositoryService personRepositoryService;

    public UpdatePersonUseCaseImpl(PersonRepositoryService personRepositoryService) {
        this.personRepositoryService = personRepositoryService;
    }

    @Override
    public void execute(Person person) {
        if(!personRepositoryService.doesPersonIdExists(person.getId())){
            throw new PersonIdNotFoundException();
        }
        personRepositoryService.updatePerson(person);
    }
}
