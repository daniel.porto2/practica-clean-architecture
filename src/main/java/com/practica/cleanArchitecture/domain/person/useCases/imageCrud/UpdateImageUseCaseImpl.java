package com.practica.cleanArchitecture.domain.person.useCases.imageCrud;

import com.practica.cleanArchitecture.domain.person.entity.Image;
import com.practica.cleanArchitecture.domain.person.entity.Person;
import com.practica.cleanArchitecture.domain.person.exceptions.ImageNotFoundException;
import com.practica.cleanArchitecture.domain.person.exceptions.PersonIdNotFoundException;
import com.practica.cleanArchitecture.domain.person.ports.PersonRepositoryService;

public class UpdateImageUseCaseImpl implements UpdateImageUseCase{

    private final PersonRepositoryService personRepositoryService;

    public UpdateImageUseCaseImpl(PersonRepositoryService personRepositoryService) {
        this.personRepositoryService = personRepositoryService;
    }

    @Override
    public void execute(Person person, Image newImage) {
        if(!personRepositoryService.doesPersonIdExists(person.getId())){
            throw new PersonIdNotFoundException();
        }
        if(!personRepositoryService.doesPersonHaveImage(person)){
            throw new ImageNotFoundException();
        }
        person.setImage(newImage);
        personRepositoryService.updateImage(person);
    }
}
