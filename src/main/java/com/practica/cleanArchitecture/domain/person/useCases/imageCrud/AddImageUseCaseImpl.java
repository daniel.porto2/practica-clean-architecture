package com.practica.cleanArchitecture.domain.person.useCases.imageCrud;

import com.practica.cleanArchitecture.domain.person.entity.Image;
import com.practica.cleanArchitecture.domain.person.entity.Person;
import com.practica.cleanArchitecture.domain.person.exceptions.ImageAlreadyExistsForPersonId;
import com.practica.cleanArchitecture.domain.person.exceptions.PersonIdNotFoundException;
import com.practica.cleanArchitecture.domain.person.ports.PersonRepositoryService;

public class AddImageUseCaseImpl implements AddImageUseCase{

    private final PersonRepositoryService personRepositoryService;

    public AddImageUseCaseImpl(PersonRepositoryService personRepositoryService) {
        this.personRepositoryService = personRepositoryService;
    }

    public Image execute(Person person, Image image) {
        if(!personRepositoryService.doesPersonIdExists(person.getId())){
            throw new PersonIdNotFoundException();
        }
        if(personRepositoryService.doesPersonHaveImage(person)){
            throw new ImageAlreadyExistsForPersonId();
        }
        person.setImage(image);
        return personRepositoryService.saveImage(person);
    }
}
