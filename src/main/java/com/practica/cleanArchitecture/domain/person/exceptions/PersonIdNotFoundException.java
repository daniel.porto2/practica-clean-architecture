package com.practica.cleanArchitecture.domain.person.exceptions;

import com.practica.cleanArchitecture.domain.shared.mainExceptions.NotFoundException;

public class PersonIdNotFoundException extends NotFoundException {
    public static final String DESCRIPTION = "Person Id not found";

    public PersonIdNotFoundException() {
        super(DESCRIPTION);
    }

    public PersonIdNotFoundException(String detail) {
        super(DESCRIPTION + ". " + detail);
    }
}
