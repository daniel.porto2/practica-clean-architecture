package com.practica.cleanArchitecture.domain.person.exceptions;

import com.practica.cleanArchitecture.domain.shared.mainExceptions.DuplicatedFieldException;

public class ImageAlreadyExistsForPersonId extends DuplicatedFieldException {
    public static final String DESCRIPTION = "An image already exists for this person";

    public ImageAlreadyExistsForPersonId() {
        super(DESCRIPTION);
    }

    public ImageAlreadyExistsForPersonId(String detail) {
        super(DESCRIPTION + ". " + detail);
    }
}
