package com.practica.cleanArchitecture.domain.person.useCases.personCrud;

import com.practica.cleanArchitecture.domain.person.entity.Person;
import com.practica.cleanArchitecture.domain.person.exceptions.PersonIdNotFoundException;
import com.practica.cleanArchitecture.domain.person.ports.PersonRepositoryService;

public class DeletePersonUseCaseImpl implements DeletePersonUseCase{
    private final PersonRepositoryService personRepositoryService;

    public DeletePersonUseCaseImpl(PersonRepositoryService personRepositoryService) {
        this.personRepositoryService = personRepositoryService;
    }

    public void execute(Person person) {
        if(!personRepositoryService.doesPersonIdExists(person.getId())){
            throw new PersonIdNotFoundException();
        }
        personRepositoryService.deletePerson(person);
    }
}
