package com.practica.cleanArchitecture.domain.person.useCases.personCrud;

import com.practica.cleanArchitecture.domain.person.entity.Person;

public interface DeletePersonUseCase {
    public void execute(Person person);
}
