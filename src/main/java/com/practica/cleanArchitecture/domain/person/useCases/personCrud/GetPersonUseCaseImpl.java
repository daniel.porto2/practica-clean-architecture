package com.practica.cleanArchitecture.domain.person.useCases.personCrud;

import com.practica.cleanArchitecture.domain.person.entity.Person;
import com.practica.cleanArchitecture.domain.person.exceptions.PersonIdNotFoundException;
import com.practica.cleanArchitecture.domain.person.ports.PersonRepositoryService;

import java.util.Optional;
import java.util.UUID;

public class GetPersonUseCaseImpl implements GetPersonUseCase{

    private final PersonRepositoryService personRepositoryService;

    public GetPersonUseCaseImpl(PersonRepositoryService personRepositoryService) {
        this.personRepositoryService = personRepositoryService;
    }

    @Override
    public Person execute(Person person) {
        if(!personRepositoryService.doesPersonIdExists(person.getId())){
            throw new PersonIdNotFoundException();
        }
        return personRepositoryService.findPersonById(person.getId());
    }
}
