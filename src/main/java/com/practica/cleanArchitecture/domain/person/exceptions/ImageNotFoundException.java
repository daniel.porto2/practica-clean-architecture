package com.practica.cleanArchitecture.domain.person.exceptions;

import com.practica.cleanArchitecture.domain.shared.mainExceptions.NotFoundException;

public class ImageNotFoundException extends NotFoundException {
    public static final String DESCRIPTION = "Image not found";

    public ImageNotFoundException() {
        super(DESCRIPTION);
    }

    public ImageNotFoundException(String detail) {
        super(DESCRIPTION + ". " + detail);
    }
}
