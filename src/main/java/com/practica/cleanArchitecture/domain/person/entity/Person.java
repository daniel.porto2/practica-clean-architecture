package com.practica.cleanArchitecture.domain.person.entity;

public class Person {
    private String id;

    private String  documentNumber, names, surnames, countryOfOrigin;

    private DocumentTypeEnum documentType;

    private int age;

    private SexEnum sex;

    private Image image;

    public Person(){

    }

    public Person(String id) {
        this.id = id;
    }

    public Person(String id, String documentNumber, String names, String surnames, String countryOfOrigin, DocumentTypeEnum documentType, int age, SexEnum sex, Image image) {
        this.id = id;
        this.documentNumber = documentNumber;
        this.names = names;
        this.surnames = surnames;
        this.countryOfOrigin = countryOfOrigin;
        this.documentType = documentType;
        this.age = age;
        this.sex = sex;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getSurnames() {
        return surnames;
    }

    public void setSurnames(String surnames) {
        this.surnames = surnames;
    }

    public String getCountryOfOrigin() {
        return countryOfOrigin;
    }

    public void setCountryOfOrigin(String countryOfOrigin) {
        this.countryOfOrigin = countryOfOrigin;
    }

    public DocumentTypeEnum getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentTypeEnum documentType) {
        this.documentType = documentType;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public SexEnum getSex() {
        return sex;
    }

    public void setSex(SexEnum sex) {
        this.sex = sex;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }
}
