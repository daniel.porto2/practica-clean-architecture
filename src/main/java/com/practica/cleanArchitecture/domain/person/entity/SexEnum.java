package com.practica.cleanArchitecture.domain.person.entity;

public enum SexEnum {
    F,
    M
}
