package com.practica.cleanArchitecture.domain.person.useCases.personCrud;

import com.practica.cleanArchitecture.domain.person.entity.Person;
import com.practica.cleanArchitecture.domain.person.exceptions.DocumentNumberAlreadyExists;
import com.practica.cleanArchitecture.domain.person.ports.PersonRepositoryService;

public class CreatePersonUseCaseImpl implements CreatePersonUseCase{

    private final PersonRepositoryService personRepositoryService;

    public CreatePersonUseCaseImpl(PersonRepositoryService personRepositoryService) {
        this.personRepositoryService = personRepositoryService;
    }

    @Override
    public Person execute(Person person) {
        if(personRepositoryService.doesDocumentNumberExists(person.getDocumentNumber())){
            throw new DocumentNumberAlreadyExists();
        }
        System.out.println(person);
        Person savedPerson = personRepositoryService.savePerson(person);
        System.out.println(savedPerson);
        return savedPerson;
    }
}
