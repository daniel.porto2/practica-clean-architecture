package com.practica.cleanArchitecture.domain.person.ports;

import com.practica.cleanArchitecture.domain.person.entity.Image;
import com.practica.cleanArchitecture.domain.person.entity.Person;

import java.util.Optional;

public interface PersonRepositoryService {
    public Person savePerson(Person person);

    public boolean doesDocumentNumberExists(String documentNumber);

    public boolean doesPersonIdExists(String id);

    public Person findPersonById(String id);

    public void updatePerson(Person person);

    public void deletePerson(Person person);

    public boolean doesPersonHaveImage(Person person);

    public Image saveImage(Person person);

    public Image getImage(Person person);

    public void updateImage(Person person);

    public void deleteImage(Person person);
}
