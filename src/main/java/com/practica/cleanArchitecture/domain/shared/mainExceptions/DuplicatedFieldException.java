package com.practica.cleanArchitecture.domain.shared.mainExceptions;

public class DuplicatedFieldException extends RuntimeException{
    public DuplicatedFieldException(String message) {
        super(message);
    }
}
