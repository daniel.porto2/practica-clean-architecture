package com.practica.cleanArchitecture.domain.shared.mainExceptions;

public class NotFoundException extends RuntimeException{
    public NotFoundException(String message) {
        super(message);
    }
}
